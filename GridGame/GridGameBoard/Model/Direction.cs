﻿namespace GridGameBoard.Model
{
	public enum Direction
	{
		Up,
		Right,
		Down,
		Left
	}
}

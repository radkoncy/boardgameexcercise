﻿using System;

namespace GridGameBoard.Interfaces
{
	public interface IBoard
	{
		int BoardHeight { get; }
		int BoardWidth { get; }

		Tuple<int, int> CurrentPosition { get; }
		void Move(int steps);
		void RotateRight();
		void RotateLeft();
	}
}

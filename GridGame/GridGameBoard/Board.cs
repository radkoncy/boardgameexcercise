﻿using GridGameBoard.Interfaces;
using GridGameBoard.Model;
using System;

namespace GridGameBoard
{
	public class Board : IBoard
	{
		public Board(int width, int height) : this(width, height, 0, 0)
		{}

		public Board(int width, int height, int x, int y)
		{
			BoardWidth = width;
			BoardHeight = height;
			CurrentPosition = new Tuple<int, int>(x, y);
		}

		public int BoardHeight { get; private set; }
		public int BoardWidth { get; private set; }
		public Tuple<int, int> CurrentPosition{ get; private set; }
		private Direction currentDirection = Direction.Up;

		public void Move(int steps)
		{
			int x = CurrentPosition.Item1;
			int y = CurrentPosition.Item2;

			switch (currentDirection)
			{
				case Direction.Up:
					y += steps;
					break;

				case Direction.Right:
					x += steps;
					break;

				case Direction.Down:
					y -= steps;
					break;

				case Direction.Left:
					x -= steps;
					break;

				default:
					throw new ArgumentOutOfRangeException();
					break;
			}

			CheckBoundaries(ref x, ref y);
			CurrentPosition = new Tuple<int, int>(x, y);
		}

		private void CheckBoundaries(ref int x, ref int y)
		{
			if (x >= BoardWidth)
				x = BoardWidth - 1;

			if (x < 0)
				x = 0;

			if (y >= BoardHeight)
				y = BoardHeight - 1;

			if (y < 0)
				y = 0;
		}

		public void RotateRight()
		{
			int directionInt = ((int)currentDirection) + 1;

			if (directionInt > 3)
				currentDirection = 0;

			currentDirection = (Direction)directionInt;
		}

		public void RotateLeft()
		{
			int directionInt = ((int)currentDirection) - 1;

			if (directionInt < 0)
				directionInt = 3;

			currentDirection = (Direction)directionInt;
		}
	}
}

﻿using GridGameBoard;
using System;

namespace GridGame
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Please provide size of the board:");
			var boardSizes = GetTwoIntsFromUser();

			Console.WriteLine("Please provide starting point:");
			var position = GetTwoIntsFromUser();

			var board = new Board(boardSizes.Item1, boardSizes.Item2, position.Item1, position.Item2);
			KeepAskingUserForNextStep(board);

			Console.WriteLine(string.Format("{0},{1}", board.CurrentPosition.Item1, board.CurrentPosition.Item2));
			Console.ReadKey();
		}

		private static void KeepAskingUserForNextStep(Board board)
		{
			var input = string.Empty;

			do
			{
				Console.WriteLine("Please provide your next move (or type Enter key to finish):");
				input = Console.ReadLine();

				if (string.Equals(input.ToLowerInvariant(), "r"))
					board.RotateRight();
				else if (string.Equals(input.ToLowerInvariant(), "l"))
					board.RotateLeft();
				else if (input.ToLowerInvariant().StartsWith("m"))
				{
					int howManySteps;
					int.TryParse(input.Substring(1), out howManySteps);

					board.Move(howManySteps);
				}

			} while (!string.IsNullOrWhiteSpace(input));
		}

		private static Tuple<int, int> GetTwoIntsFromUser()
		{
			var twoStrings = Console.ReadLine().Split(',');

			if (twoStrings.Length != 2)
				throw new ArgumentException("wrong input");

			int a = int.Parse(twoStrings[0]);
			int b = int.Parse(twoStrings[1]);

			return new Tuple<int, int>(a, b);
		}
	}
}

﻿using GridGameBoard;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GridGameBoardTests
{
	[TestClass]
	public class BoardTest
	{

		[TestMethod]
		public void BoardSizeTest()
		{
			VerifyBoardSize(5, 5);
			VerifyBoardSize(1, 1);
			VerifyBoardSize(1, 1000);
		}

		private static void VerifyBoardSize(int expectedHeight, int expectedWidth)
		{
			var board = new Board(expectedWidth, expectedHeight);

			Assert.AreEqual(board.BoardHeight, expectedHeight, string.Format("Testing width, board size: {0}x{1}", expectedWidth, expectedHeight));
			Assert.AreEqual(board.BoardWidth, expectedWidth, string.Format("Testing width, board size: {0}x{1}", expectedWidth, expectedHeight));
		}

		[TestMethod]
		public void DefaultPosition()
		{
			CheckDefaultPosition(5, 5);
			CheckDefaultPosition(1, 1);
			CheckDefaultPosition(1, 100);
		}

		private static void CheckDefaultPosition(int width, int height)
		{
			var board = new Board(width, height);
			CheckPosition(0, 0, board);
		}

		private static void CheckPosition(int x, int y, Board board)
		{
			Assert.AreEqual(board.CurrentPosition.Item1, x, string.Format("Testing position X (should be {0}), board size: {1}x{2}", x, board.BoardWidth, board.BoardHeight));
			Assert.AreEqual(board.CurrentPosition.Item2, y, string.Format("Testing position Y (should be {0}), board size: {1}x{2}", y, board.BoardWidth, board.BoardHeight));
		}

		[TestMethod]
		public void RotateLeft()
		{
			var board = new Board(5, 5);
			board.RotateRight();
			board.Move(1);

			CheckPosition(1, 0, board);
		}

		[TestMethod]
		public void RotateRight()
		{
			var board = new Board(5, 5);
			board.RotateRight();
			board.Move(1);

			CheckPosition(1, 0, board);
		}
		
		[TestMethod]
		public void MoveUpTest()
		{
			var board = new Board(5, 5);
			board.Move(3);

			CheckPosition(0, 3, board);
		}

		[TestMethod]
		public void MoveDownTest()
		{
			var board = new Board(5, 5, 1, 1);
			board.RotateRight();
			board.RotateRight();
			board.Move(1);

			CheckPosition(1, 0, board);
		}

		[TestMethod]
		public void MoveLeftTest()
		{
			var board = new Board(5, 5, 1, 1);
			board.RotateLeft();
			board.Move(1);

			CheckPosition(0, 1, board);
		}

		[TestMethod]
		public void TestFromExcercise()
		{
			var board = new Board(5, 5, 1, 1);
			board.RotateRight();
			board.Move(2);
			board.RotateRight();
			board.Move(3);
			board.RotateLeft();
			board.Move(1);
			board.RotateLeft();
			board.Move(3);

			CheckPosition(4, 3, board);
		}
	}
}
